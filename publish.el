;; ;; publish.el --- Org blog -*- lexical-binding: t -*-

;; Author: Joshua Hwang
;; URL: https://gitlab.com/joshuahwang24/joshuahwang24.gitlab.io

;;; Commentary:
;; This script will convert the org-mode files in the source directory
;; into the appropriate html files.

;;; Code:

(require 'ox-publish)
(require 'seq)

;; RSS and atom webfeed generator for Emacs
(add-to-list 'load-path "emacs-webfeeder")
(if (require 'webfeeder nil 'noerror)
    (call-process "git" nil nil nil "-C" "emacs-webfeeder" "pull")
  (call-process "git" nil nil nil "clone" "https://gitlab.com/ambrevar/emacs-webfeeder")
  (require 'webfeeder))

(defvar josh/repository "https://gitlab.com/joshuahwang24/joshuahwang24.gitlab.io")
(defvar josh/root (expand-file-name "."))

(setq org-publish-use-timestamps-flag t
      org-publish-timestamp-directory "./")

(setq make-backup-files nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-email t
      org-export-with-date t
      org-export-with-tags 'not-in-toc)

(defun josh/git-first-date (file)
  "Return the first commit date of FILE. Format is %Y-%m-%d."
  (with-temp-buffer
    (call-process "git" nil t nil "log" "--reverse" "--date=short" "--pretty=format:%cd" file)
    (goto-char (point-min))
		(buffer-substring-no-properties (line-beginning-position) (line-end-position))))

(defun josh/git-last-date (file)
  "Return the last commit date of FILE. Format is %Y-%m-%d."
  (with-output-to-string
    (with-current-buffer standard-output
      (call-process "git" nil t nil "log" "-1" "--date=short" "--pretty=format:%cd" file))))

(defun josh/blog-preview (file)
	(with-temp-buffer
		(insert-file-contents file)
		(goto-char (point-min))
		(let ((beg (+ 1 (re-search-forward "^#\\+BEGIN_PREVIEW$")))
					(end (progn (re-search-forward "^#\\+END_PREVIEW$")
											(match-beginning 0))))
			(buffer-substring beg end))))

(defun josh/org-html-format-spec (info)
  "Return format specification for preamble and postamble."
  (let* ((timestamp-format (plist-get info :html-metadata-timestamp-format))
         (file (plist-get info :input-file))
         (meta-date (org-export-data (org-export-get-date info timestamp-format)
                                     info))
         (creation-date (if (string= "" meta-date)
                          (josh/git-first-date file)
                        meta-date))
         (last-update-date (josh/git-last-date file)))
    `((?t . ,(org-export-data (plist-get info :title) info))
     (?s . ,(org-export-data (plist-get info :subtitle) info))
     (?d . ,creation-date)
     (?T . ,(format-time-string timestamp-format))
     (?a . ,(org-export-data (plist-get info :author) info))
     (?e . ,(mapconcat
             (lambda (e) (format "" e e))
             (split-string (plist-get info :email) ",+ *")
             ", "))
     (?c . ,(plist-get info :creator))
     (?C . ,last-update-date)
     (?v . ,(or (plist-get info :html-validation-link) ""))
     (?u . ,(if (string= creation-date last-update-date)
                 creation-date
               (format "%s (<a href=%s>Last update: %s</a>)"
                       creation-date
                       (format "%s/commits/master/%s" josh/repository (file-relative-name file josh/root))
                       last-update-date))))))
(advice-add 'org-html-format-spec :override 'josh/org-html-format-spec)

(defun josh/preamble (info)
  "Return preamble as a string."
  (let* ((file (plist-get info :input-file))
         (prefix (file-relative-name (expand-file-name "source" josh/root)
                                     (file-name-directory file))))
		(format "<a href=\"%1$s/index.html\">Home</a>
<a href=\"%1$s/blog.html\">Blog</a>
" prefix)))

(setq org-html-postamble t
      org-html-postamble-format `(("en" ,(concat "<p>Joshua Hwang ©</p>
<p>All Rights Reserved - 2020</p>")))
      org-html-preamble #'josh/preamble
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-htmlize-output-type nil
      org-html-doctype "html5")

(defun josh/org-publish-sitemap (title list)
  (setcdr list (seq-filter
                (lambda (file)
                  (string-match "file:[^ ]*/index.org" (car file)))
                (cdr list)))
  (concat "#+TITLE: " title "\n"
          "#+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">"
					"\n"
          "#+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"fonts/\">"
					"\n"
          "#+HTML_HEAD: <link rel=\"atom.xml\" type=\"application/atom+xml\" rel=\"alternate\" title=\"Bugi Idris' homepage\">"
					"\n"
          (org-list-to-org list)))

(defun josh/org-publish-find-date (file project)
  (let ((file (org-publish--expand-file-name file project)))
    (or (org-publish-cache-get-file-property file :date nil t)
        (org-publish-cache-set-file-property
         file :date
         (let ((date (org-publish-find-property file :date project)))
           (let ((ts (and (consp date) (assq 'timestamp date))))
             (and ts
                  (let ((value (org-element-interpret-data ts)))
                    (and (org-string-nw-p value)
                         (org-time-string-to-time value))))))))))

(defun josh/org-publish-sitemap-entry (entry style project)
  (cond ((not (directory-name-p entry))
         (let* ((meta-date (josh/org-publish-find-date entry project))
                (file (expand-file-name entry
                                        (org-publish-property :base-directory project)))
                (creation-date (if (not meta-date)
                                   (josh/git-first-date file)
                                 (format-time-string "%Y-%m-%d" meta-date)))
                (last-date (josh/git-last-date file)))
           (format "[[file:%s][%s]]^{ (%s)}"
                   entry
                   (org-publish-find-title entry project)
                   (if (string= creation-date last-date)
                       creation-date
                     (format "%s, updated %s" creation-date last-date)))))
         ((eq style 'tree)
          (capitalize (file-name-nondirectory (directory-file-name entry))))
         (t entry)))

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "./source/"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/"
             :sitemap-format-entry #'josh/org-publish-sitemap-entry
             :auto-sitemap t
             :sitemap-title "Blog"
             :sitemap-filename "blog.org"
             :sitemap-style 'list
             :sitemap-function #'josh/org-publish-sitemap
             :sitemap-sort-files 'anti-chronologically
             :with-toc nil
             :html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\">
<link href=\"../atom.xml\" type=\"application/atom+xml\" rel=\"alternate\" title=\"Joshua Hwang's homepage\">")
       (list "site-static"
             :base-directory "source/"
             :exclude "\\.org\\'"
             :base-extension 'any
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site-cert"
             :base-directory ".well-known"
             :exclude "public/"
             :base-extension 'any
             :publishing-directory "./public/.well-known"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org"))))


(defun josh/publish ()
  (org-publish-all)
  (setq webfeeder-default-author "Joshua Hwang <joshuahwang24@gmail.com")
  (webfeeder-build
   "rss.xml"
   "./public"
   "https://jxsh.blog/"
   (delete "index.html"
           (mapcar (lambda (f) (replace-regexp-in-string ".*/public/" "" f))
                   (directory-files-recursively "public" "index.html")))
   :builder 'webfeeder-make-rss
   :title "Joshua Hwang's homepage"
   :description "(Abstract Abstract)")
  (webfeeder-build
   "atom-xml"
   "./public"
   "https://jxsh.blog/"
   (delete "index.html"
           (mapcar (lambda (f) (replace-regexp-in-string ".*/public/" "" f))
                   (directory-files-recursively "public" "index.html")))
   :title "Joshua Hwang's homepage"
   :description "(Abstract Abstract)"))

(provide 'publish)
;; publish.el ends here
